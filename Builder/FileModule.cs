﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Builder
{
    public class FileNode : TreeNode
    {
        public string path;
        public bool ready = false; // true ... podadresare jiz nacteny
    }
    
    public class FileModule
    {
        private TreeView fileTree;
        
        public FileModule (TreeView fileTree0)
        {
            fileTree = fileTree0;
            fileTree.BeforeExpand += fileTree_BeforeExpand;
        }
        
        public void displayDrives ()
        {
            fileTree.Nodes.Clear ();
            DriveInfo[] roots = DriveInfo.GetDrives ();
            foreach (DriveInfo r in roots)
                displayBranch (fileTree.Nodes, r.RootDirectory);
        }

        private void displayBranch (TreeNodeCollection target, DirectoryInfo dir, int levels = 2)
        {
            FileNode node = new FileNode();
            node.path = dir.FullName;
            node.ready = false;
            node.Text = dir.Name;
            node.ToolTipText = node.path;
            node.ForeColor = Color.Blue;
            target.Add (node);
            displayDetail (node, dir, levels);
        }

        private void displayDetail (FileNode node, DirectoryInfo dir, int levels)
        {
            if (levels > 1)
                try
                {
                    foreach (DirectoryInfo d in dir.GetDirectories ())
                        displayBranch (node.Nodes, d, levels - 1);
                    node.ready = true;
                } catch (Exception e)
                {
                    TreeNode t = new TreeNode();
                    t.Text = e.ToString ();
                    t.ToolTipText = e.StackTrace;
                    t.ForeColor = Color.Red;
                    node.Nodes.Add (t);
                }
        }

        private void fileTree_BeforeExpand (object sender, TreeViewCancelEventArgs e)
        {
            foreach (TreeNode n in e.Node.Nodes)
            {
                FileNode node = n as FileNode;
                if (node != null && !node.ready)
                {
                    try
                    {
                        DirectoryInfo dir = new DirectoryInfo (node.path);
                        displayDetail (node, dir, 2);
                    } 
                    catch (Exception)
                    {
                    }
                }
            }
        }
        
    } // end of class
} // end of namespace
