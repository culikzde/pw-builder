﻿using System;
using System.Windows.Forms;
using System.Xml;

namespace Builder
{
    public class XmlModule
    {
        private TreeView treeView;
        private PropertyGrid propGrid;
        
        public XmlModule (TreeView treeView0, PropertyGrid propGrid0)
        {
            treeView = treeView0;
            propGrid = propGrid0;
            
            treeView.AfterSelect += treeView_AfterSelect;
        }
        
        /* Document Object Model */
        
        private void showDom (XmlNode node, TreeNode top)
        {
            TreeNode n = new TreeNode ();
            n.Text = node.Name;
            n.Tag = node;

            if (top == null)
                treeView.Nodes.Add (n);
            else
                top.Nodes.Add(n);

            foreach (XmlNode child in node.ChildNodes)
                showDom (child, n);
        }

        public void readDom (string fileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load (fileName);
            showObj (doc.DocumentElement);
            showDom (doc.DocumentElement, null);
        }
        
        /* XML Text Reader */

        public void readSimpleXml (string fileName)
        {
            XmlTextReader f = new XmlTextReader (fileName);
            TreeNode current = null;
            while (f.Read())
            {
                showObj (f);
                if (f.NodeType == XmlNodeType.Element)
                {
                    TreeNode n = new TreeNode();
                    n.Text = "<" + f.Name + ">";

                    if (current == null)
                        treeView.Nodes.Add(n);
                    else
                        current.Nodes.Add(n);

                    current = n;

                    if (f.HasAttributes)
                    {
                        for (int i = 0; i < f.AttributeCount; i++)
                        {
                            f.MoveToAttribute(i);
                            TreeNode a = new TreeNode();
                            a.Text = f.Name + " = " + f.Value;
                            current.Nodes.Add(a);
                        }
                        f.MoveToElement();
                    }

                    if (f.IsEmptyElement)
                        current = current.Parent;
                }
                else if (f.NodeType == XmlNodeType.EndElement)
                {
                    current = current.Parent;
                }
                else if (f.NodeType == XmlNodeType.Text)
                {
                    TreeNode n = new TreeNode();
                    n.Text = f.Value;
                    current.Nodes.Add(n);
                }
            }
        }

        private void showObj (object p)
        {
            propGrid.SelectedObject = p;
        }
        
        private void treeView_AfterSelect (object sender, TreeViewEventArgs e)
        {
            TreeNode node = treeView.SelectedNode;
            propGrid.SelectedObject = node.Tag;
        }
        
    } // end of class
} // end of namespace
