﻿using System;
using System.Reflection;
using System.Windows.Forms;

namespace Builder
{
    public class ReflectionModule
    {
        private TreeView reflectionTree;
        private PropertyGrid propGrid;
        private DataGridView reflectionGrid;
        
        public ReflectionModule (TreeView reflectionTree0, 
                                 PropertyGrid propGrid0,
                                 DataGridView reflectionGrid0)
        {
            reflectionTree = reflectionTree0;
            propGrid = propGrid0;
            reflectionGrid = reflectionGrid0;
            
            reflectionTree.AfterSelect += reflectionTree_AfterSelect;
        }
        
        public void display (object obj)
        {
            addStructure (reflectionTree.Nodes, obj);
        }
        
        private void addStructure (TreeNodeCollection target, object obj)
        {
           TreeNode node = new TreeNode ();
           node.Tag = obj;
           string typ = obj.GetType().ToString();
           
           if (obj is Control)
           {
              Control c = obj as Control;
              node.Text = c.Name + " : " + typ;
              
              foreach (Control item in c.Controls)
                 addStructure (node.Nodes, item);
           }
           else
           {
              node.Text = obj.ToString () + " : " + typ;
           }
           
           target.Add (node);
        }
       
        void reflectionTree_AfterSelect (object sender, TreeViewEventArgs e)
        {
           object obj = e.Node.Tag;
           Type typ = obj.GetType ();
           
           propGrid.SelectedObject = obj;
           
           foreach (PropertyInfo prop in typ.GetProperties ())
           {
              string name = prop.Name;
              object value = null;
              if (prop.CanRead)
              {
                 value = prop.GetValue (obj, null); /* second parameter required from .Net 4.5 */
              }
              object [] line = new object [] {name, value};
              reflectionGrid.Rows.Add (line);
           }
        }

    } // end of class
} // end of namespace
