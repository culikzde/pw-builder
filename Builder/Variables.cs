﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Builder
{
    public class Variables
    {
        public int X;
        public int Y;
        public Form Win;
    }
}

/*
private async void runMenu_ClickAsync(object sender, EventArgs e)
{
    // dynamic obj = new ExpandoObject ();
    // obj.Abc = "Klm";
    // propGrid.SelectedObject = obj;

    // propGrid.SelectedObject = new { Abc = "abc" };
    
    // 1 + 2
    // X + Y
    // Win.Width
    // Win.Text
    // Win.Text = "Abc";

    string source = input.Text;
    var variables = new Variables { X = 1, Y = 2, Win = this };
    // object result = await CSharpScript.EvaluateAsync (source, globals : variables);
    var state = await CSharpScript.RunAsync(source, globals: variables);
    var result = state.ReturnValue;
    var compilation = state.Script.GetCompilation();

    output.AppendText(source + " -> " + result + "\n");
    foreach (var v in state.Variables)
        output.AppendText("   " + v.Name + " = " + v.Value + "\n");
    foreach (SyntaxTree t in compilation.SyntaxTrees)
        ;
    InOutTabControl.SelectedTab = outputPage;
}
*/

// Tools > Options > Environment > Fonts and Colors
// Tools > Options > Text Editor > C# > CodeStyle > Formatting > Spacing

// using Microsoft.CodeAnalysis.CSharp.Scripting;
// https://github.com/dotnet/roslyn/wiki/Scripting-API-Samples

// Tools > NuGet Package Manager > Package Manager Console
// Install-Package Microsoft.CodeAnalysis.CSharp.Scripting
