﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Builder
{
    public partial class MainWindow : Form
    {
        private CodeModule codeModule;
        private FileModule fileModule;
        private ReflectionModule reflectionModule;
        private XmlModule xmlModule;
        
        public MainWindow ()
        {
            InitializeComponent ();

            createColorPanels (new List<String> { "red", "green", "blue", "yellow", "orange", "lime", "cornflowerblue" });

            createComponent (typeof (Record), typeof (RecordView));
            createComponent (typeof (Field), typeof (FieldView));
            createComponent (typeof (Button));
            createComponent (typeof (Panel));
            createComponent (typeof (TreeView));
            createComponent (typeof (ListView));
            createComponent (typeof (TextBox));
            createComponent (typeof (DataGrid));

            designer.AllowDrop = true;
            treeView.AllowDrop = true;
            
            codeModule = new CodeModule (codeTree, propGrid);
            fileModule = new FileModule (fileTree);
            reflectionModule = new ReflectionModule (structureTree, propGrid, reflectionGrid);
            xmlModule = new XmlModule (xmlTree, propGrid);
            
            test ();
        }
        
        private void test ()
        {
            var orig = new ExpandoObject ();
            dynamic obj = orig;
            obj.color = Color.Orange;
            
            var data = obj as IDictionary<string, Object>;
            data.Add ("name", "expando");
            
            TypeDescriptor.AddAttributes (obj, new CategoryAttribute ("a"));
            TypeDescriptor.AddAttributes (obj.color, new CategoryAttribute ("b"));
            
            reflectionModule.display (obj);
            leftTabs.SelectedTab = structureTab;
            structureTree.SelectedNode = structureTree.Nodes[0];
        }

        private void createColorPanels (List<String> colorNames)
        {
            foreach (var colorName in colorNames)
            {
                Color color = Color.FromName (colorName);
                ColorPanel p = new ColorPanel (color, colorName);
                // int n = colorPage.Controls.Count;
                // p.Left = n * p.Width + (n + 1) * p.Left;
                // colorPage.Controls.Add (p);
                colorToolbar.Items.Add (p);
            }
        }

        private void createComponent (Type type0, Type view0 = null)
        {
            Factory f = new Factory (type0, view0);

            int n = componentPage.Controls.Count;
            f.Left = f.Left + n * (f.Width + f.Left);
            componentPage.Controls.Add (f);
        }

        private void designer_DragEnter (object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent (typeof (Color)))
                e.Effect = DragDropEffects.Move;
            // else if (e.Data.GetDataPresent (DataFormats.Text))
            //    e.Effect = DragDropEffects.Copy;
            // else if (e.Data.GetDataPresent ("System.Windows.Forms.Button"))
            //     e.Effect = DragDropEffects.Copy;
            else if (e.Data.GetDataPresent ("Builder.Factory"))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void designer_DragDrop (object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent ("Builder.Factory"))
            {
                Factory factory = e.Data.GetData ("Builder.Factory") as Factory;
                Point location = designer.PointToClient (new Point (e.X, e.Y));
                createObject (factory, null, location);
            }
        }

        private void treeView_DragEnter (object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent ("Builder.Factory"))
                e.Effect = DragDropEffects.Copy;
        }

        private void treeView_DragDrop (object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent ("Builder.Factory"))
            {
                Factory factory = e.Data.GetData ("Builder.Factory") as Factory;
                Point pt = treeView.PointToClient (new Point (e.X, e.Y));
                TreeNode target = treeView.GetNodeAt (pt);
                createObject (factory, target, new Point (0, 0));
            }
        }

        private void createObject (Factory factory, TreeNode target, Point location)
        {
            Object obj = factory.createInstance ();

            MyNode node = new MyNode ();
            node.Text = obj.ToString ();
            node.Tag = obj;
            node.connect ();

            if (target == null)
                treeView.Nodes.Add (node);
            else
                target.Nodes.Add (node);

            if (obj is Control)
            {
                Control ctl = obj as Control;
                if (location != null)
                {
                    ctl.Left = location.X;
                    ctl.Top = location.Y;
                }

                new Adapter (ctl, this);

                designer.Controls.Add (ctl);
            }

        }

        private void treeView_NodeMouseClick (object sender, TreeNodeMouseClickEventArgs e)
        {
            SelectObject (e.Node.Tag);
        }

        public void SelectObject (object obj)
        {
            if (obj is Common)
                obj = (obj as Common).recallData();

            // obj = new ObjectWrapper (obj);
            propGrid.SelectedObject = obj;
        }

        private void readData (TextWriter stream)
        {

        }

        private void writeData (TextWriter stream)
        {
            foreach (TreeNode node in treeView.Nodes)
            {
                object obj = node.Tag;
                if (obj is Common)
                    obj = (obj as Common).recallData ();

                XmlSerializer writer = new XmlSerializer (obj.GetType ());
                writer.Serialize (stream, obj);
            }
        }

        private void open_Click (object sender, EventArgs e)
        {
            if (openDialog.ShowDialog () == DialogResult.OK)
            {
                StreamReader stream = new StreamReader (openDialog.FileName);
                // XmlSerializer reader = new XmlSerializer (typeof (MyObject));
                // obj = (MyObject)reader.Deserialize (stream);
                stream.Close ();

                // AddObject (obj);
            }
        }

        private void save_Click (object sender, EventArgs e)
        {
            if (saveDialog.ShowDialog () == DialogResult.OK)
            {
                StreamWriter stream = new StreamWriter (saveDialog.FileName);
                // XmlSerializer writer = new XmlSerializer (typeof (MyObject));
                // writer.Serialize (stream, obj);
                writeData (stream);
                stream.Close ();
            }
        } 

        private void viewXml_Click (object sender, EventArgs e)
        {
            StringWriter stream = new StringWriter ();
            writeData (stream);
            xmlEdit.Text = stream.ToString ();
            // xmlModule
        }

        private void viewData_Click (object sender, EventArgs e)
        {
            new DataWindow ().Show ();
        }

        private void quit_Click(object sender , EventArgs e)
        {
            Close ();
        }
        
        /* File Tree */

        void viewFiles_Click (object sender, EventArgs e)
        {
            leftTabs.SelectedTab = fileTab;
            fileModule.displayDrives ();
        }
        
        /* Reflection */
        
        void viewReflection_Click (object sender, EventArgs e)
        {
           leftTabs.SelectedTab = structureTab;
           rightTabs.SelectedTab = reflectionTab;
           
           structureTree.Nodes.Clear ();
           reflectionModule.display (this);
           structureTree.SelectedNode = structureTree.Nodes[0];
           structureTree.Nodes[0].Expand ();
        }
        
        /* Compilation */
        
        void compile_Click (object sender, EventArgs e)
        {
            leftTabs.SelectedTab = codeTab;
            centralTabs.SelectedTab = editorTab;
            
            info.Clear ();
            codeModule.compile (editor, info);
        }
        
    } // end of class
} // end of namespace
