﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Builder
{
    public partial class FieldView : UserControl, Common
    {
        public FieldView ()
        {
            InitializeComponent ();
        }

        Field data;
        public void setupData(object data0) { data = data0 as Field; }
        public object recallData() { return data; }

        public override string ToString() { return "Field " + (data != null ? data.Name : ""); }
    }
}
