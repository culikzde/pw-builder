﻿using System.Drawing; // Color

namespace Builder
{
    public class Field
    {
        public string Name { get; set; }
        public Color Color { get; set; }

        public static Color BasicColor () { return Color.CornflowerBlue; }    }
}
