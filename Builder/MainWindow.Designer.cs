﻿namespace Builder
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.quitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.viewDataMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewXmlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.viewFilesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewReflectionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.compileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.leftTabs = new System.Windows.Forms.TabControl();
            this.treeTab = new System.Windows.Forms.TabPage();
            this.treeView = new System.Windows.Forms.TreeView();
            this.fileTab = new System.Windows.Forms.TabPage();
            this.fileTree = new System.Windows.Forms.TreeView();
            this.xmlTab = new System.Windows.Forms.TabPage();
            this.xmlTree = new System.Windows.Forms.TreeView();
            this.structureTab = new System.Windows.Forms.TabPage();
            this.structureTree = new System.Windows.Forms.TreeView();
            this.codeTab = new System.Windows.Forms.TabPage();
            this.codeTree = new System.Windows.Forms.TreeView();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.centralTabs = new System.Windows.Forms.TabControl();
            this.designerTab = new System.Windows.Forms.TabPage();
            this.designer = new System.Windows.Forms.Panel();
            this.xmlEditTab = new System.Windows.Forms.TabPage();
            this.xmlEdit = new System.Windows.Forms.TextBox();
            this.editorTab = new System.Windows.Forms.TabPage();
            this.editor = new System.Windows.Forms.TextBox();
            this.rightTabs = new System.Windows.Forms.TabControl();
            this.propTab = new System.Windows.Forms.TabPage();
            this.propGrid = new System.Windows.Forms.PropertyGrid();
            this.reflectionTab = new System.Windows.Forms.TabPage();
            this.reflectionGrid = new System.Windows.Forms.DataGridView();
            this.nameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.palette = new System.Windows.Forms.TabControl();
            this.componentPage = new System.Windows.Forms.TabPage();
            this.colorPage = new System.Windows.Forms.TabPage();
            this.colorToolbar = new System.Windows.Forms.ToolStrip();
            this.info = new System.Windows.Forms.TextBox();
            this.openDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveDialog = new System.Windows.Forms.SaveFileDialog();
            this.mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.leftTabs.SuspendLayout();
            this.treeTab.SuspendLayout();
            this.fileTab.SuspendLayout();
            this.xmlTab.SuspendLayout();
            this.structureTab.SuspendLayout();
            this.codeTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.centralTabs.SuspendLayout();
            this.designerTab.SuspendLayout();
            this.xmlEditTab.SuspendLayout();
            this.editorTab.SuspendLayout();
            this.rightTabs.SuspendLayout();
            this.propTab.SuspendLayout();
            this.reflectionTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reflectionGrid)).BeginInit();
            this.palette.SuspendLayout();
            this.colorPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.viewMenu,
            this.compileMenu});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(1008, 24);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openMenuItem,
            this.saveMenuItem,
            this.separator1,
            this.quitMenuItem});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // openMenuItem
            // 
            this.openMenuItem.Name = "openMenuItem";
            this.openMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openMenuItem.Text = "&Open";
            this.openMenuItem.Click += new System.EventHandler(this.open_Click);
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveMenuItem.Text = "&Save";
            this.saveMenuItem.Click += new System.EventHandler(this.save_Click);
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            this.separator1.Size = new System.Drawing.Size(100, 6);
            // 
            // quitMenuItem
            // 
            this.quitMenuItem.Name = "quitMenuItem";
            this.quitMenuItem.Size = new System.Drawing.Size(103, 22);
            this.quitMenuItem.Text = "&Quit";
            this.quitMenuItem.Click += new System.EventHandler(this.quit_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewDataMenuItem,
            this.viewXmlMenuItem,
            this.separator2,
            this.viewFilesMenuItem,
            this.viewReflectionMenuItem});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(44, 20);
            this.viewMenu.Text = "View";
            // 
            // viewDataMenuItem
            // 
            this.viewDataMenuItem.Name = "viewDataMenuItem";
            this.viewDataMenuItem.Size = new System.Drawing.Size(127, 22);
            this.viewDataMenuItem.Text = "Data";
            this.viewDataMenuItem.Click += new System.EventHandler(this.viewData_Click);
            // 
            // viewXmlMenuItem
            // 
            this.viewXmlMenuItem.Name = "viewXmlMenuItem";
            this.viewXmlMenuItem.Size = new System.Drawing.Size(127, 22);
            this.viewXmlMenuItem.Text = "Xml";
            this.viewXmlMenuItem.Click += new System.EventHandler(this.viewXml_Click);
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            this.separator2.Size = new System.Drawing.Size(124, 6);
            // 
            // viewFilesMenuItem
            // 
            this.viewFilesMenuItem.Name = "viewFilesMenuItem";
            this.viewFilesMenuItem.Size = new System.Drawing.Size(127, 22);
            this.viewFilesMenuItem.Text = "Files";
            this.viewFilesMenuItem.Click += new System.EventHandler(this.viewFiles_Click);
            // 
            // viewReflectionMenuItem
            // 
            this.viewReflectionMenuItem.Name = "viewReflectionMenuItem";
            this.viewReflectionMenuItem.Size = new System.Drawing.Size(127, 22);
            this.viewReflectionMenuItem.Text = "Reflection";
            this.viewReflectionMenuItem.Click += new System.EventHandler(this.viewReflection_Click);
            // 
            // compileMenu
            // 
            this.compileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compileMenuItem});
            this.compileMenu.Name = "compileMenu";
            this.compileMenu.Size = new System.Drawing.Size(64, 20);
            this.compileMenu.Text = "Compile";
            // 
            // compileMenuItem
            // 
            this.compileMenuItem.Name = "compileMenuItem";
            this.compileMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.compileMenuItem.Size = new System.Drawing.Size(138, 22);
            this.compileMenuItem.Text = "&Compile";
            this.compileMenuItem.Click += new System.EventHandler(this.compile_Click);
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 739);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1008, 22);
            this.statusBar.TabIndex = 1;
            this.statusBar.Text = "statusStrip1";
            // 
            // toolBar
            // 
            this.toolBar.Location = new System.Drawing.Point(0, 24);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(1008, 25);
            this.toolBar.TabIndex = 2;
            this.toolBar.Text = "toolStrip1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 49);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel1.Controls.Add(this.palette);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.info);
            this.splitContainer1.Size = new System.Drawing.Size(1008, 690);
            this.splitContainer1.SplitterDistance = 550;
            this.splitContainer1.TabIndex = 6;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 77);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.leftTabs);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(1008, 473);
            this.splitContainer2.SplitterDistance = 165;
            this.splitContainer2.TabIndex = 5;
            // 
            // leftTabs
            // 
            this.leftTabs.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.leftTabs.Controls.Add(this.treeTab);
            this.leftTabs.Controls.Add(this.fileTab);
            this.leftTabs.Controls.Add(this.xmlTab);
            this.leftTabs.Controls.Add(this.structureTab);
            this.leftTabs.Controls.Add(this.codeTab);
            this.leftTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftTabs.Location = new System.Drawing.Point(0, 0);
            this.leftTabs.Multiline = true;
            this.leftTabs.Name = "leftTabs";
            this.leftTabs.SelectedIndex = 0;
            this.leftTabs.Size = new System.Drawing.Size(165, 473);
            this.leftTabs.TabIndex = 6;
            // 
            // treeTab
            // 
            this.treeTab.Controls.Add(this.treeView);
            this.treeTab.Location = new System.Drawing.Point(23, 4);
            this.treeTab.Name = "treeTab";
            this.treeTab.Padding = new System.Windows.Forms.Padding(3);
            this.treeTab.Size = new System.Drawing.Size(138, 465);
            this.treeTab.TabIndex = 0;
            this.treeTab.Text = "Tree";
            this.treeTab.UseVisualStyleBackColor = true;
            // 
            // treeView
            // 
            this.treeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.Location = new System.Drawing.Point(3, 3);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(132, 459);
            this.treeView.TabIndex = 6;
            this.treeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView_NodeMouseClick);
            this.treeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeView_DragDrop);
            this.treeView.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeView_DragEnter);
            // 
            // fileTab
            // 
            this.fileTab.Controls.Add(this.fileTree);
            this.fileTab.Location = new System.Drawing.Point(23, 4);
            this.fileTab.Name = "fileTab";
            this.fileTab.Padding = new System.Windows.Forms.Padding(3);
            this.fileTab.Size = new System.Drawing.Size(138, 465);
            this.fileTab.TabIndex = 1;
            this.fileTab.Text = "Files";
            this.fileTab.UseVisualStyleBackColor = true;
            // 
            // fileTree
            // 
            this.fileTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fileTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileTree.Location = new System.Drawing.Point(3, 3);
            this.fileTree.Name = "fileTree";
            this.fileTree.Size = new System.Drawing.Size(132, 459);
            this.fileTree.TabIndex = 0;
            // 
            // xmlTab
            // 
            this.xmlTab.Controls.Add(this.xmlTree);
            this.xmlTab.Location = new System.Drawing.Point(23, 4);
            this.xmlTab.Name = "xmlTab";
            this.xmlTab.Padding = new System.Windows.Forms.Padding(3);
            this.xmlTab.Size = new System.Drawing.Size(138, 465);
            this.xmlTab.TabIndex = 4;
            this.xmlTab.Text = "XML";
            this.xmlTab.UseVisualStyleBackColor = true;
            // 
            // xmlTree
            // 
            this.xmlTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.xmlTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xmlTree.Location = new System.Drawing.Point(3, 3);
            this.xmlTree.Name = "xmlTree";
            this.xmlTree.Size = new System.Drawing.Size(132, 459);
            this.xmlTree.TabIndex = 0;
            // 
            // structureTab
            // 
            this.structureTab.Controls.Add(this.structureTree);
            this.structureTab.Location = new System.Drawing.Point(23, 4);
            this.structureTab.Name = "structureTab";
            this.structureTab.Padding = new System.Windows.Forms.Padding(3);
            this.structureTab.Size = new System.Drawing.Size(138, 465);
            this.structureTab.TabIndex = 3;
            this.structureTab.Text = "Reflection";
            this.structureTab.UseVisualStyleBackColor = true;
            // 
            // structureTree
            // 
            this.structureTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.structureTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.structureTree.Location = new System.Drawing.Point(3, 3);
            this.structureTree.Name = "structureTree";
            this.structureTree.Size = new System.Drawing.Size(132, 459);
            this.structureTree.TabIndex = 0;
            // 
            // codeTab
            // 
            this.codeTab.Controls.Add(this.codeTree);
            this.codeTab.Location = new System.Drawing.Point(23, 4);
            this.codeTab.Name = "codeTab";
            this.codeTab.Padding = new System.Windows.Forms.Padding(3);
            this.codeTab.Size = new System.Drawing.Size(138, 465);
            this.codeTab.TabIndex = 2;
            this.codeTab.Text = "Code";
            this.codeTab.UseVisualStyleBackColor = true;
            // 
            // codeTree
            // 
            this.codeTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.codeTree.Location = new System.Drawing.Point(3, 3);
            this.codeTree.Name = "codeTree";
            this.codeTree.Size = new System.Drawing.Size(132, 459);
            this.codeTree.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.centralTabs);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.rightTabs);
            this.splitContainer3.Size = new System.Drawing.Size(839, 473);
            this.splitContainer3.SplitterDistance = 664;
            this.splitContainer3.TabIndex = 0;
            // 
            // centralTabs
            // 
            this.centralTabs.Controls.Add(this.designerTab);
            this.centralTabs.Controls.Add(this.xmlEditTab);
            this.centralTabs.Controls.Add(this.editorTab);
            this.centralTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centralTabs.Location = new System.Drawing.Point(0, 0);
            this.centralTabs.Name = "centralTabs";
            this.centralTabs.SelectedIndex = 0;
            this.centralTabs.Size = new System.Drawing.Size(664, 473);
            this.centralTabs.TabIndex = 1;
            // 
            // designerTab
            // 
            this.designerTab.Controls.Add(this.designer);
            this.designerTab.Location = new System.Drawing.Point(4, 22);
            this.designerTab.Name = "designerTab";
            this.designerTab.Padding = new System.Windows.Forms.Padding(3);
            this.designerTab.Size = new System.Drawing.Size(656, 447);
            this.designerTab.TabIndex = 0;
            this.designerTab.Text = "Designer";
            this.designerTab.UseVisualStyleBackColor = true;
            // 
            // designer
            // 
            this.designer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.designer.Location = new System.Drawing.Point(3, 3);
            this.designer.Name = "designer";
            this.designer.Size = new System.Drawing.Size(650, 441);
            this.designer.TabIndex = 0;
            this.designer.DragDrop += new System.Windows.Forms.DragEventHandler(this.designer_DragDrop);
            this.designer.DragEnter += new System.Windows.Forms.DragEventHandler(this.designer_DragEnter);
            // 
            // xmlEditTab
            // 
            this.xmlEditTab.Controls.Add(this.xmlEdit);
            this.xmlEditTab.Location = new System.Drawing.Point(4, 22);
            this.xmlEditTab.Name = "xmlEditTab";
            this.xmlEditTab.Padding = new System.Windows.Forms.Padding(3);
            this.xmlEditTab.Size = new System.Drawing.Size(656, 447);
            this.xmlEditTab.TabIndex = 2;
            this.xmlEditTab.Text = "XML";
            this.xmlEditTab.UseVisualStyleBackColor = true;
            // 
            // xmlEdit
            // 
            this.xmlEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xmlEdit.Location = new System.Drawing.Point(3, 3);
            this.xmlEdit.Multiline = true;
            this.xmlEdit.Name = "xmlEdit";
            this.xmlEdit.Size = new System.Drawing.Size(650, 441);
            this.xmlEdit.TabIndex = 0;
            // 
            // editorTab
            // 
            this.editorTab.Controls.Add(this.editor);
            this.editorTab.Location = new System.Drawing.Point(4, 22);
            this.editorTab.Name = "editorTab";
            this.editorTab.Padding = new System.Windows.Forms.Padding(3);
            this.editorTab.Size = new System.Drawing.Size(656, 447);
            this.editorTab.TabIndex = 1;
            this.editorTab.Text = "Editor";
            this.editorTab.UseVisualStyleBackColor = true;
            // 
            // editor
            // 
            this.editor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editor.Location = new System.Drawing.Point(3, 3);
            this.editor.Multiline = true;
            this.editor.Name = "editor";
            this.editor.Size = new System.Drawing.Size(650, 441);
            this.editor.TabIndex = 0;
            this.editor.Text = resources.GetString("editor.Text");
            // 
            // rightTabs
            // 
            this.rightTabs.Alignment = System.Windows.Forms.TabAlignment.Right;
            this.rightTabs.Controls.Add(this.propTab);
            this.rightTabs.Controls.Add(this.reflectionTab);
            this.rightTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightTabs.Location = new System.Drawing.Point(0, 0);
            this.rightTabs.Multiline = true;
            this.rightTabs.Name = "rightTabs";
            this.rightTabs.SelectedIndex = 0;
            this.rightTabs.Size = new System.Drawing.Size(171, 473);
            this.rightTabs.TabIndex = 7;
            // 
            // propTab
            // 
            this.propTab.Controls.Add(this.propGrid);
            this.propTab.Location = new System.Drawing.Point(4, 4);
            this.propTab.Name = "propTab";
            this.propTab.Padding = new System.Windows.Forms.Padding(3);
            this.propTab.Size = new System.Drawing.Size(144, 465);
            this.propTab.TabIndex = 0;
            this.propTab.Text = "Properties";
            this.propTab.UseVisualStyleBackColor = true;
            // 
            // propGrid
            // 
            this.propGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propGrid.Location = new System.Drawing.Point(3, 3);
            this.propGrid.Name = "propGrid";
            this.propGrid.Size = new System.Drawing.Size(138, 459);
            this.propGrid.TabIndex = 6;
            // 
            // reflectionTab
            // 
            this.reflectionTab.Controls.Add(this.reflectionGrid);
            this.reflectionTab.Location = new System.Drawing.Point(4, 4);
            this.reflectionTab.Name = "reflectionTab";
            this.reflectionTab.Padding = new System.Windows.Forms.Padding(3);
            this.reflectionTab.Size = new System.Drawing.Size(144, 465);
            this.reflectionTab.TabIndex = 1;
            this.reflectionTab.Text = "Reflection";
            this.reflectionTab.UseVisualStyleBackColor = true;
            // 
            // reflectionGrid
            // 
            this.reflectionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.reflectionGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameCol,
            this.valueCol});
            this.reflectionGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reflectionGrid.Location = new System.Drawing.Point(3, 3);
            this.reflectionGrid.Name = "reflectionGrid";
            this.reflectionGrid.Size = new System.Drawing.Size(138, 459);
            this.reflectionGrid.TabIndex = 0;
            // 
            // nameCol
            // 
            this.nameCol.HeaderText = "Name";
            this.nameCol.Name = "nameCol";
            // 
            // valueCol
            // 
            this.valueCol.HeaderText = "Value";
            this.valueCol.Name = "valueCol";
            // 
            // palette
            // 
            this.palette.Controls.Add(this.componentPage);
            this.palette.Controls.Add(this.colorPage);
            this.palette.Dock = System.Windows.Forms.DockStyle.Top;
            this.palette.Location = new System.Drawing.Point(0, 0);
            this.palette.Name = "palette";
            this.palette.SelectedIndex = 0;
            this.palette.Size = new System.Drawing.Size(1008, 77);
            this.palette.TabIndex = 4;
            // 
            // componentPage
            // 
            this.componentPage.Location = new System.Drawing.Point(4, 22);
            this.componentPage.Name = "componentPage";
            this.componentPage.Padding = new System.Windows.Forms.Padding(3);
            this.componentPage.Size = new System.Drawing.Size(1000, 51);
            this.componentPage.TabIndex = 0;
            this.componentPage.Text = "Components";
            this.componentPage.UseVisualStyleBackColor = true;
            // 
            // colorPage
            // 
            this.colorPage.Controls.Add(this.colorToolbar);
            this.colorPage.Location = new System.Drawing.Point(4, 22);
            this.colorPage.Name = "colorPage";
            this.colorPage.Padding = new System.Windows.Forms.Padding(3);
            this.colorPage.Size = new System.Drawing.Size(1000, 51);
            this.colorPage.TabIndex = 1;
            this.colorPage.Text = "Colors";
            this.colorPage.UseVisualStyleBackColor = true;
            // 
            // colorToolbar
            // 
            this.colorToolbar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colorToolbar.Location = new System.Drawing.Point(3, 3);
            this.colorToolbar.Margin = new System.Windows.Forms.Padding(0, 0, 4, 0);
            this.colorToolbar.Name = "colorToolbar";
            this.colorToolbar.Padding = new System.Windows.Forms.Padding(0);
            this.colorToolbar.Size = new System.Drawing.Size(994, 45);
            this.colorToolbar.TabIndex = 0;
            this.colorToolbar.Text = "toolStrip1";
            // 
            // info
            // 
            this.info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.info.Location = new System.Drawing.Point(0, 0);
            this.info.Multiline = true;
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(1008, 136);
            this.info.TabIndex = 0;
            // 
            // openDialog
            // 
            this.openDialog.FileName = "openFileDialog1";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 761);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolBar);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "MainWindow";
            this.Text = "Builder";
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.leftTabs.ResumeLayout(false);
            this.treeTab.ResumeLayout(false);
            this.fileTab.ResumeLayout(false);
            this.xmlTab.ResumeLayout(false);
            this.structureTab.ResumeLayout(false);
            this.codeTab.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.centralTabs.ResumeLayout(false);
            this.designerTab.ResumeLayout(false);
            this.xmlEditTab.ResumeLayout(false);
            this.xmlEditTab.PerformLayout();
            this.editorTab.ResumeLayout(false);
            this.editorTab.PerformLayout();
            this.rightTabs.ResumeLayout(false);
            this.propTab.ResumeLayout(false);
            this.reflectionTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.reflectionGrid)).EndInit();
            this.palette.ResumeLayout(false);
            this.colorPage.ResumeLayout(false);
            this.colorPage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem quitMenuItem;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStrip toolBar;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.PropertyGrid propGrid;
        private System.Windows.Forms.TabControl palette;
        private System.Windows.Forms.TabPage componentPage;
        private System.Windows.Forms.TabPage colorPage;
        private System.Windows.Forms.TextBox info;
        private System.Windows.Forms.Panel designer;
        private System.Windows.Forms.ToolStrip colorToolbar;
        private System.Windows.Forms.ToolStripMenuItem openMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
        private System.Windows.Forms.OpenFileDialog openDialog;
        private System.Windows.Forms.SaveFileDialog saveDialog;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem viewDataMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewXmlMenuItem;
        private System.Windows.Forms.TabControl leftTabs;
        private System.Windows.Forms.TabPage treeTab;
        private System.Windows.Forms.TabPage fileTab;
        private System.Windows.Forms.TabControl centralTabs;
        private System.Windows.Forms.TabPage designerTab;
        private System.Windows.Forms.TabPage editorTab;
        private System.Windows.Forms.TabControl rightTabs;
        private System.Windows.Forms.TabPage propTab;
        private System.Windows.Forms.TabPage reflectionTab;
        private System.Windows.Forms.TextBox editor;
        private System.Windows.Forms.ToolStripMenuItem compileMenu;
        private System.Windows.Forms.ToolStripMenuItem compileMenuItem;
        private System.Windows.Forms.TabPage codeTab;
        private System.Windows.Forms.TreeView codeTree;
        private System.Windows.Forms.TabPage structureTab;
        private System.Windows.Forms.TabPage xmlTab;
        private System.Windows.Forms.TreeView fileTree;
        private System.Windows.Forms.TreeView xmlTree;
        private System.Windows.Forms.TreeView structureTree;
        private System.Windows.Forms.TabPage xmlEditTab;
        private System.Windows.Forms.TextBox xmlEdit;
        private System.Windows.Forms.ToolStripMenuItem viewReflectionMenuItem;
        private System.Windows.Forms.DataGridView reflectionGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueCol;
        private System.Windows.Forms.ToolStripSeparator separator1;
        private System.Windows.Forms.ToolStripSeparator separator2;
        private System.Windows.Forms.ToolStripMenuItem viewFilesMenuItem;
    }
}

