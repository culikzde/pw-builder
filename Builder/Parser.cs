﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Dynamic;

namespace Builder
{
    public class Parser : Lexer
    {
        public Parser()
        {
        }
        
        public void readClass ()
        {
            checkKeyword ("class");
            checkSeparator ('{');
            while (! isSeparator ('}'))
            {
               string type = readIdentifier ("Type name expected");
               string name = readIdentifier ("Variable identifier expected");
               checkSeparator (';');
            }
            checkSeparator ('}');
        }
        
        public void readData ()
        {
            string type_name = readIdentifier ();
            string block_name = "";
            if (isIdentifier ())
                block_name = readIdentifier ();
            
            // dynamic data = new ExpandoObject ();
            dynamic orig = new ExpandoObject ();
            var data = orig as IDictionary<string, Object>;
            orig.color = Color.CornflowerBlue;
            TypeDescriptor.AddAttributes (orig, new CategoryAttribute ("a"));
            TypeDescriptor.AddAttributes (orig.color, new CategoryAttribute ("b"));
            
            checkSeparator ('{');
            while (! isSeparator ('}'))
            {
               string name = readIdentifier ("Variable identifier expected");
               checkSeparator ('=');

               string value = "";
               if (isIdentifier ())
                   value = readIdentifier ();
               else if (isNumber ())
                   value = readNumber ();
               else if (isReal ())
                   value = readReal ();
               else if (isCharacter ())
                   value = readCharacter ();
               else if (isString ())
                  value = readString ();
               else
                   error ("Value expected");
               
               data.Add (name, value);
               
               checkSeparator (';');
            }
            checkSeparator ('}');
        }
    }
}
