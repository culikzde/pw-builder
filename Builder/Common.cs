﻿namespace Builder
{
    interface Common
    {
        void setupData (object data);
        object recallData ();
    }
}
